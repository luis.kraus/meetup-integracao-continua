# NoteWorx

#### Install dependencies
```bash
$ docker-compose run --rm application npm install

// OU

$ npm run docker:install
```

#### Build application
```bash
$ docker-compose run --rm application npm run build

// OU

$ npm run docker:build
```

#### Run application
```bash
$ docker-compose up frontend

// OU

$ npm run docker:start
```

#### Run unit tests
```bash
$ docker-compose run --rm application npm test

// OU

$ npm run docker:unit-test
```

#### Run eslint
```bash
$ docker-compose run --rm application npm run eslint

// OU

$ npm run docker:eslint
```

#### Run E2E Tests on Linux
```bash
$ NODE_ENV=docker docker-compose run codeceptjs ./node_modules/.bin/codeceptjs run

// OU

$ npm run docker:e2e-test
```

#### Run E2E Tests on Windows
```bash
$ set NODE_ENV=docker && docker-compose run --rm codeceptjs ./node_modules/.bin/codeceptjs run

// OU

$ npm run docker:e2e-test-win
```